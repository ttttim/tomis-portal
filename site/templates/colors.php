<?php namespace ProcessWire;?>

<style media="screen">
  ul{
    list-style: none;
  }
  li + li{
      border-top: 1px solid #ccc;
  }
</style>

<h1><?=$page->title?></h1>

<ul>
  <?php foreach ($page->color_table as $color): ?>
    <li style="color:<?=$color->hex?>">
      <h2><?=$color->name?></h2>
      <ul>
        <?php
        foreach ($color as $key => $value){
          if ($key != 'name' && $value != '') {
            echo "<li>$value</li>";
          }
        }
        ?>
      </ul>

    </li>
  <?php endforeach; ?>

</ul>
